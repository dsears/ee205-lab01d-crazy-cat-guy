///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab01d - Crazy Cat Guy - EE 205 - Spr 2022
///
/// @file    crazyCatGuy.c
/// @version 1.0 - Initial version
///
/// Compile: $ gcc -o crazyCatGuy crazyCatGuy.c
///
/// Usage:  crazyCatGuy n
///   n:  Sum the digits from 1 to n
///
/// Result:
///   The sum of the digits from 1 to n is XX
///
/// Example:
///   $ crazyCatGuy 6
///   The sum of the digits from 1 to 6 is 21
///
/// Dane Sears dsears@hawaii.edu
/// 12_JAN_2022
///////////////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <stdlib.h>

int i = 0;
int collars = 0;

int main( int argc, char* argv[] ) {
   int n = atoi( argv[1] );


   while( i <= n)
   {
   
      collars= collars + i;
      i++;
   

   }

   printf("The sum of the digits from 1 to %d is %d\n", n, collars);
   return 0;
}
